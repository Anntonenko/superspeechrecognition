//
//  WordViewController.swift
//  SuperSpeechRecognition
//
//  Created by getTrickS2 on 3/8/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class WordViewController: UIViewController {

    var acceptedWord = (word: "",transcription: "")
    var utterance: AVSpeechUtterance!
    let synthesizer = AVSpeechSynthesizer()
    
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var transcriptionLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recognizeButton: UIButton!
    @IBOutlet weak var recognizedLabel: UILabel!
    
    @IBAction func playWord(_ sender: Any) {
        synthesizer.speak(utterance)
    }
    
    @IBAction func recognizeSpeech(_ sender: Any) {
        synthesizer.speak(utterance)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        wordLabel.text = acceptedWord.word
        transcriptionLabel.text = acceptedWord.transcription
        playButton.layer.cornerRadius = 5
        recognizeButton.layer.borderWidth = 1
        recognizeButton.layer.borderColor = view.tintColor.cgColor
        recognizeButton.layer.cornerRadius = 5
        
        utterance = AVSpeechUtterance(string: acceptedWord.word)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
